import {listTimeZones, searchTimeZones} from '../repositories/timezones'
import {sendError} from "./index";

const list = (req, res) => {
  listTimeZones()
    .then(list => res.send(list))
    .catch(error => sendError(res, error))
}

const search = (req, res) => {
  searchTimeZones(req.body.name)
    .then(list => res.send(list))
    .catch(error => sendError(res, error))
}

export {
  list,
  search
}
