export const sendError = (res, error) => {
  console.log(error)
  res.status(404).json(error)
}
