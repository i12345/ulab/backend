import express from 'express'
import morgan from 'morgan'
import chalk from 'chalk'
import debug from 'debug'
import bodyParser from 'body-parser'
import cors from 'cors'
import routes from './routes'

const app = express()

var whitelist = [process.env.FRONTEND_URL]
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(null, false)
    }
  }
}
app.use(cors(corsOptions))

app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extend: false }))

routes(app, '/api/v1')

const port = process.env.PORT || 3000
app.listen(port, () => {
  const de = debug(process.env.LOG || 'app')
  de(`backend running @ port ${chalk.green(port)}`)
})
