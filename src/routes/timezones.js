import express from "express";
import {list, search} from "../controllers/timezones";

const router = express.Router()

router.route('').get(list)
router.route('/search').post(search)

export default router
