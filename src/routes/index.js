import timezones from "./timezones";

const routes = (app, baseUrl) => {
  app.use(`${baseUrl}/timezones`, timezones)
}

export default routes
