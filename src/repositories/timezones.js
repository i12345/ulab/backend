import fs from 'fs'
import path from 'path'
import xml2js from 'xml2js'
import {first, filter, forEach} from 'lodash'


const readTimeZonesFromFile = async () => {
  return fs.readFileSync(path.join(__dirname, '../../data/timezones.xml'))
}

const convertXml2Json = async (xml) => {
  const json = await xml2js.parseStringPromise(xml)
  const { TimeZones: { TimeZone : timezones } } = json
  return timezones
}

const processJson = (timezones) => {

  let list = []
  forEach(timezones, timezone => {
    const {Id, Name, Hours, Mins, Secs} = timezone
    let name = first(Name) || ""
    let indexOfUTC = Math.max(0, name.indexOf("UTC") ?? 0)
    name = name.substr(0, indexOfUTC).trim()
    const names = name.split(",")
    forEach(names, (name, index) => {
      list.push({
        id: `${first(Id) || ""}-${index}`,
        name: name.trim(),
        hours : parseInt(first(Hours) || ""),
        mins : parseInt(first(Mins) || ""),
        secs : parseInt(first(Secs) || "")
      })
    })
  })

  return list
}

const listTimeZones = async () => {
  const xml = await readTimeZonesFromFile()
  const json = await convertXml2Json(xml)
  return processJson(json)
}

const searchTimeZones = async (search) => {
  const xml = await readTimeZonesFromFile()
  let timezones = await convertXml2Json(xml)
  timezones = processJson(timezones)
  timezones = filter(timezones, timezone => {
    const name = timezone.name
    return name ? name.toLowerCase().includes(search.toLowerCase()) : false
  })
  return timezones
}

export {
  listTimeZones,
  searchTimeZones
}
